using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class ConnectToServer : MonoBehaviourPunCallbacks
{
    [Header("Spinning Circle")]
    public RectTransform imgRect;
    public float rotateSpeed = -200f;

    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    // Update is called once per frame
    void Update()
    {
        imgRect.Rotate(0f, 0f, rotateSpeed * Time.deltaTime);
    }

    public override void OnConnectedToMaster()
    {
        StartCoroutine(LoadScene(1));
    }

    public IEnumerator LoadScene(int sceneId)
    {
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene(sceneId);
    }
}
