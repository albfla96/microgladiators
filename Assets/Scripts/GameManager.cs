﻿using Photon.Pun;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviourPunCallbacks
{
    public static GameManager INSTANCE;

    public bool IsRoundOver { get; set; }
    public bool IsGameOver { get; set; }
    public bool IsTie { get; set; }

    //[SerializeField]
    private GameObject endGamePanel;

    [SerializeField]
    private Text playerWinsText;

    //[SerializeField]
    private Text score;

    [SerializeField]
    private GameObject[] crates;

    [SerializeField]
    private GameObject cratePrefab;
    
    public int rounds = 3;

    private GameObject[] players;
    public Player Player1 { get; set; }
    public Player Player2 { get; set; }

    //public Joystick joystick;

    public Weapon pistol;
    public Weapon machineGun;
    public Weapon laser;
    public Weapon rocketLauncher;

    private void Awake()
    {
        if (INSTANCE == null)
        {
            INSTANCE = this;
        }
        else
        {
            if (INSTANCE != this)
            {
                Destroy(gameObject);
            }
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        //players = GameObject.FindGameObjectsWithTag("Player");
        //Player1 = players[0].GetComponent<Player>();
        //Player1.ResetPosition();
        //Player2 = players[1].GetComponent<Player>();
        //Player2.ResetPosition();

        pistol.ammo = Mathf.Infinity;
        IsGameOver = true;
        IsRoundOver = true;
        //endGamePanel.SetActive(true);
    }

    public void ExitGame()
    {
        PhotonNetwork.LoadLevel(2);
    }

    public void StartGame()
    {
        ClearCrates();
        playerWinsText.gameObject.SetActive(false);
        IsGameOver = false;
        IsRoundOver = false;
        endGamePanel.SetActive(false);
        GetComponent<CountDown>().ResetTimer();
        //TODO reset using Photon Network
        foreach (GameObject play in players)
        {
            play.GetComponent<Player>().ResetScore();
            play.GetComponent<Player>().ResetPosition();
        }
        ClearBullets();
        ShowScore();
        StartCoroutine(Spawn());
    }

    public void GameOver(Player winner)
    {
        StopCoroutine(Spawn());
        ClearBullets();
        ShowScore();
        IsGameOver = true;
        endGamePanel.SetActive(true);
        SetWinnerText(winner);
    }

    public void RoundOver(Player winner)
    {
        ClearCrates();
        ClearBullets();
        IsRoundOver = true;
        
        foreach (GameObject play in players)
        {
            play.GetComponent<Player>().ResetPosition();
        }

        if (winner == null)
        {
            IsTie = true;
            foreach (GameObject play in players)
            {
                play.GetComponent<Player>().Score++;
            }
            IsTie = false;

            if (Player1.Score > Player2.Score && Player1.Score >= rounds)
            {
                GameOver(Player1);
            }
            else if (Player1.Score > Player2.Score && Player1.Score < rounds)
            {
                RoundOver(Player1);
            }
            else
            {
                if (Player1.Score < Player2.Score && Player2.Score >= rounds)
                {
                    GameOver(Player2);
                }
                else if (Player1.Score < Player2.Score && Player2.Score < rounds)
                {
                    RoundOver(Player2);
                }
                else
                {
                    if (Player1.Score == Player2.Score && Player1.Score >= rounds)
                    {
                        GameOver(null);
                    }
                    else if (Player1.Score == Player2.Score && Player1.Score < rounds)
                    {
                        IsRoundOver = false;
                        GetComponent<CountDown>().ResetTimer();
                    }
                }
            }
        }
        else
        {
            if (winner.Score < rounds)
            {
                IsRoundOver = false;
                GetComponent<CountDown>().ResetTimer();
            }
            else
            {
                GameOver(winner);
            }
        }

        ShowScore();
    }

    private void SetWinnerText(Player winner)
    {
        ShowScore();
        if (winner == null)
        {
            // draw
            playerWinsText.gameObject.SetActive(true);
            playerWinsText.text = "Oh no, it's a draw";
        }
        else
        {
            // winner
            playerWinsText.gameObject.SetActive(true);
            playerWinsText.text = winner.name + " wins";
        }
    }

    private void ShowScore()
    {
        score.text = Player1.Score + " - " + Player2.Score;
    }

    private void ClearBullets()
    {
        var bullets = GameObject.FindGameObjectsWithTag("Bullet");
        foreach (var bullet in bullets)
        {
            Destroy(bullet.gameObject);
        }
    }

    public Player GetWinner(Player loser)
    {
        if (loser == Player1)
        {
            return Player2;
        }

        return Player1;
    }

    private IEnumerator Spawn()
    {
        while(!IsGameOver)
        {
            int timer = Random.Range(7, 10);
            yield return new WaitForSeconds(timer);
            foreach (var crate in crates)
            {
                if (crate.transform.childCount == 0)
                {
                    var spawnedCrate = Instantiate(cratePrefab, crate.transform.position, Quaternion.identity);
                    spawnedCrate.gameObject.transform.SetParent(crate.transform);
                    break;
                }
            }
        }
    }

    private void ClearCrates()
    {
        foreach (var crate in crates)
        {
            foreach (Transform child in crate.transform)
            {
                Destroy(child.gameObject);
            }
        }
    }

    public void IncreaseRounds()
    {
        rounds = 5;
    }
}
