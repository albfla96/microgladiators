using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SpawnPlayers : MonoBehaviour
{
    public static SpawnPlayers INSTANCE;

    public GameObject player;
    public Transform[] playerSpawnPoints;
    private Photon.Realtime.Player[] allPlayers;
    private int myNumberInRoom;
    private PhotonView myPV;

    private void Awake()
    {
        if (INSTANCE == null)
        {
            INSTANCE = this;
        }
        else
        {
            if (INSTANCE != this)
            {
                Destroy(gameObject);
            }
        }
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        allPlayers = PhotonNetwork.PlayerList;
        myPV = player.GetComponent<PhotonView>();

        foreach (var p in allPlayers)
        {
            if (p != PhotonNetwork.LocalPlayer)
            {
                Debug.Log(p.NickName);
                Debug.Log(p.IsMasterClient);
                myNumberInRoom++;
            }
        }

        Debug.Log("Is PV mine? : " + myPV.IsMine);
        if (myNumberInRoom<2)
        {
            Debug.Log("PV MINE!");

            player = PhotonNetwork.Instantiate(player.name, SpawnPlayers.INSTANCE.playerSpawnPoints[myNumberInRoom].position, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
