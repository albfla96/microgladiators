﻿using UnityEngine;

public class Player : MonoBehaviour
{
    //public KeyCode left, right, up, down;
    //public KeyCode key; // TODO: Debug key

    //public GameObject playerPosition;

    private CharacterHealth characterHealth;

    [SerializeField]
    private float moveSpeed = 2f;

    public Weapon weapon { get; set; }

    [SerializeField]
    private Transform firePointUp;
    [SerializeField]
    private Transform firePointDown;
    [SerializeField]
    private Transform firePointLeft;
    [SerializeField]
    private Transform firePointRight;

    public Transform FirePoint { get; set; }

    private int score = 0;
    public int Score
    {
        get
        {
            return score;
        }
        set
        {
            //if (score < GameManager.INSTANCE.rounds)
            //{
            //    score++;
            //}
            //if (!GameManager.INSTANCE.IsTie)
            //{
            //    if (score >= GameManager.INSTANCE.rounds)
            //    {
            //        GameManager.INSTANCE.GameOver(this);
            //    }
            //    else
            //    {
            //        GameManager.INSTANCE.RoundOver(this);
            //    }
            //}
        }
    }

    public float MoveSpeed
    {
        get
        {
            return moveSpeed;
        }
        set
        {
            moveSpeed = value;
        }
    }

    private void Awake()
    {
        characterHealth = GetComponent<CharacterHealth>();
    }

    private void Start()
    {
        SetDefaultWeapon();
    }

    public void TakeDamage(float damage, bool trap)
    {
        //if (!GameManager.INSTANCE.IsGameOver)
        //{
        //    characterHealth.DealDamage(damage, trap);
        //}
    }

    public void ResetPosition()
    {
        //this.transform.position = playerPosition.transform.position;
        characterHealth.ResetHealth();
    }

    public void ResetScore()
    {
        score = 0;
    }

    public void SetDefaultWeapon()
    {
        //weapon = GameManager.INSTANCE.pistol;
    }

    private void Update()
    {
        //if (Input.GetKeyDown(key) && !GameManager.INSTANCE.IsGameOver)
        //{
        //    Score++;
        //}
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Crate"))
        {
            getRandomWeapon();
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.tag.Equals("EasterEgg"))
        {
            GameManager.INSTANCE.IncreaseRounds();
            Destroy(collision.gameObject.transform.parent.gameObject);
        }
    }

    private void getRandomWeapon()
    {
        //int randomWeapon = Random.Range(0, 3);
        //if(randomWeapon == 0)
        //{
        //    weapon = GameManager.INSTANCE.machineGun;
        //}
        //else if(randomWeapon == 1)
        //{
        //    weapon = GameManager.INSTANCE.laser;
        //}
        //else if(randomWeapon == 2)
        //{
        //    weapon = GameManager.INSTANCE.rocketLauncher;
        //}
        //GetComponent<WeaponScript>().weapon = weapon;
        //GetComponent<WeaponScript>().ResetAmmo();
    }

    public void SetFirePointUp()
    {
        FirePoint = firePointUp;
    }

    public void SetFirePointDown()
    {
        FirePoint = firePointDown;
    }

    public void SetFirePointLeft()
    {
        FirePoint = firePointLeft;
    }

    public void SetFirePointRight()
    {
        FirePoint = firePointRight;
    }
}
