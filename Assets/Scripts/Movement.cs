﻿using UnityEngine;
using Photon.Pun;

public class Movement : MonoBehaviour
{
	private Player player;
	private Animator animator;
	//private PhotonView view;

    private void Start()
	{
		
		
		//view = GetComponent<PhotonView>();
	}

	public void MoveRight()
    {
		PhotonView view = GetComponent<PhotonView>();
		Debug.Log("Right view, " + view.gameObject.name);
		Debug.Log(view.IsMine);
		if (view.IsMine)
		{
			animator.SetBool("MoveLeft", false);
			animator.SetBool("MoveUp", false);
			animator.SetBool("MoveDown", false);

			
			player.SetFirePointRight();

			if (!animator.GetBool("MoveRight"))
			{
				GetComponent<Rigidbody2D>().AddForce(Vector2.right * player.MoveSpeed);
				animator.SetBool("MoveRight", true);
			}
		}
	}

	public void MoveLeft()
    {
		PhotonView view = GetComponent<PhotonView>();
		animator = GetComponent<Animator>();
		player = GetComponent<Player>();
		Debug.Log(view.IsMine);
		if (!view.IsMine)
		{
			animator.SetBool("MoveRight", false);
			animator.SetBool("MoveDown", false);
			animator.SetBool("MoveUp", false);

			animator.SetBool("MoveLeft", true);
			player.SetFirePointLeft();
			GetComponent<Rigidbody2D>().AddForce(Vector2.left * player.MoveSpeed);
		}
	}

	public void MoveUp()
	{
		PhotonView view = GetComponent<PhotonView>();
		Debug.Log("Up view, " + view.gameObject.name);
		Debug.Log(view.IsMine);
		if (view.IsMine)
		{
			animator.SetBool("MoveRight", false);
			animator.SetBool("MoveDown", false);
			animator.SetBool("MoveLeft", false);

			animator.SetBool("MoveUp", true);
			player.SetFirePointUp();
			GetComponent<Rigidbody2D>().AddForce(Vector2.up * player.MoveSpeed);
		}
	}

	public void MoveDown()
	{
		PhotonView view = GetComponent<PhotonView>();
		Debug.Log(view.IsMine);
		if (view.IsMine)
		{
			animator.SetBool("MoveRight", false);
			animator.SetBool("MoveLeft", false);
			animator.SetBool("MoveUp", false);

			animator.SetBool("MoveDown", true);
			player.SetFirePointDown();
			GetComponent<Rigidbody2D>().AddForce(Vector2.down * player.MoveSpeed);
		}
	}

	public void Stop()
    {
		PhotonView view = GetComponent<PhotonView>();
		Debug.Log(view.IsMine);

		if (view.IsMine)
		{
			animator.SetBool("MoveLeft", false);
			animator.SetBool("MoveRight", false);
			animator.SetBool("MoveDown", false);
			animator.SetBool("MoveUp", false);

			GetComponent<Rigidbody2D>().velocity = Vector2.zero;

			transform.position += Vector3.zero;
		}
    }
}