﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterHealth : MonoBehaviour
{
    [SerializeField]
    private float healthPoints;

    public Slider healthBar;
    public GameObject healthBarPosition;

    private float currentHealth;

    private void Start()
    {
        currentHealth = healthPoints;
        healthBar.value = CalculateHealth();
    }

    private void Update()
    {
        healthBar.transform.position = healthBarPosition.transform.position;
    }

    private float CalculateHealth()
    {
        return currentHealth / healthPoints;
    }

    public void DealDamage(float damageValue, bool trap)
    {
        currentHealth -= damageValue;
        healthBar.value = CalculateHealth();
        if (currentHealth <= 0)
        {
            Die(trap);
        }
    }

    public void ResetHealth()
    {
        Start();
    }

    public void Die(bool trap)
    {
        currentHealth = 0;
        //Destroy(this.gameObject);
        if (!trap)
        {
            GameManager.INSTANCE.GetWinner(this.GetComponent<Player>()).Score++;
        }
        else
        {
            GameManager.INSTANCE.GetWinner(this.GetComponent<Player>() == GameManager.INSTANCE.Player1 ? GameManager.INSTANCE.Player1 : GameManager.INSTANCE.Player2).Score++;
        }
    }
}
