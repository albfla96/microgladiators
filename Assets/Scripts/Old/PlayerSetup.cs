using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSetup : MonoBehaviourPunCallbacks, IPunObservable
{
    CharacterController characterController;
    public Camera playerCamera;

    public Behaviour[] playerComponents;

    // Start is called before the first frame update
    void Start()
    {
        var isLocalPlayer = photonView.IsMine;

        foreach (var component in playerComponents)
        {
            component.enabled = isLocalPlayer;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            Debug.Log("Test Write");
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
            stream.SendNext(characterController.velocity);

            stream.SendNext(playerCamera.transform.position);
            stream.SendNext(playerCamera.transform.rotation);
        }
        else
        {
            Debug.Log("Test Read");
            transform.position = (Vector3)stream.ReceiveNext();
            transform.rotation = (Quaternion)stream.ReceiveNext();
            playerCamera.transform.position = (Vector3)stream.ReceiveNext();
            playerCamera.transform.rotation = (Quaternion)stream.ReceiveNext();
        }
    }
}
