﻿using UnityEngine;

public class MaceTrap : MonoBehaviour
{
    public float damage;
    public float rotationSpeed;
    private Player player;

    private void Update()
    {
        RotateMace();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            player = collision.gameObject.GetComponent<Player>();
            player.TakeDamage(damage, true);
        }
    }

    private void RotateMace()
    {
        if(this.gameObject.name.Equals("SwingingMaceTrap"))
        {
            Vector3 reference = GameObject.FindGameObjectWithTag("MaceAnchorRotation").transform.position;
            Vector3 axis = new Vector3(0, 0, -1);
            transform.RotateAround(reference, axis, 180f * Time.deltaTime * rotationSpeed);
        }
        else if (this.gameObject.name.Equals("SwingingMaceTrap2"))
        {
            Vector3 reference = GameObject.FindGameObjectWithTag("MaceAnchorRotation2").transform.position;
            Vector3 axis = new Vector3(0, 0, -1);
            transform.RotateAround(reference, axis, 180f * Time.deltaTime * rotationSpeed);
        }
    }
}
