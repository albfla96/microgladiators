﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CountDown : MonoBehaviour
{
    [SerializeField]
    private Text timer;

    [SerializeField]
    private float totalTimeUnmodified = 120f;

    private float totalTime;
    private GameManager gameManager;
    private bool canTick = false;
    private bool blinkingStarted = false;

    private void Awake()
    {
        gameManager = GetComponent<GameManager>();
    }

    private void Start()
    {
        ResetTimer();
    }

    private void Update()
    {
        if (!gameManager.IsRoundOver && !gameManager.IsGameOver)
        {
            totalTime -= Time.deltaTime;
            UpdateTimer(totalTime);

            if (totalTime <= 0)
            {
                gameManager.RoundOver(null);
            }
        }
        
        if (gameManager.IsGameOver)
        {
            int minutes = Mathf.FloorToInt(totalTimeUnmodified / 60f);
            int seconds = Mathf.RoundToInt(totalTimeUnmodified % 60f);

            string formatedSeconds = seconds.ToString();

            timer.text = minutes.ToString("00") + ":" + seconds.ToString("00");
        }
    }

    public void UpdateTimer(float totalSeconds)
    {
        int minutes = Mathf.FloorToInt(totalSeconds / 60f);
        int seconds = Mathf.RoundToInt(totalSeconds % 60f);

        string formatedSeconds = seconds.ToString();

        if (seconds == 60)
        {
            seconds = 0;
            minutes += 1;
        }

        if (minutes == 0 && seconds <= 20)
        {
            canTick = true;
            if (!blinkingStarted)
            {
                StartCoroutine(TimerBlink());
            }
        }
        else
        {
            canTick = false;
            timer.color = Color.white;
        }

        timer.text = minutes.ToString("00") + ":" + seconds.ToString("00");
    }

    public void ResetTimer()
    {
        totalTime = totalTimeUnmodified;
    }

    private IEnumerator TimerBlink()
    {
        blinkingStarted = true;
        while (canTick)
        {
            yield return new WaitForSeconds(.4f);
            timer.color = Color.red;
            yield return new WaitForSeconds(.4f);
            timer.color = Color.white;
        }
        blinkingStarted = false;
    }
}
