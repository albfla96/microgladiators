﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Player enemyPlayer;
    private Animator animator;

    public Player CurrentPlayer { get; set; }
    public float BulletDamage { get; set; }
    public float BulletSpeed { get; set; }
    public Sprite xAxis { get; set; }
    public Sprite yAxis { get; set; }
    public Sprite xNegativeAxis { get; set; }
    public Sprite yNegativeAxis { get; set; }

    private void Start()
    {
        animator = GetComponent<Animator>();
        GetComponent<SpriteRenderer>().sprite = GetSprite();
        GetComponent<Rigidbody2D>().velocity = GetDirection() * BulletSpeed;
    }

    private Sprite GetSprite()
    {
        Vector3 localPosition = CurrentPlayer.FirePoint.transform.localPosition;

        if (localPosition.x > 0)
        {
            return xAxis;
        }
        if (localPosition.x < 0)
        {
            return xNegativeAxis;
        }
        if (localPosition.y > 0)
        {
            return yAxis;
        }
        if (localPosition.y < 0)
        {
            return yNegativeAxis;
        }

        return xAxis;
    }
    
    private Vector2 GetDirection()
    {
        Vector3 localPosition = CurrentPlayer.FirePoint.transform.localPosition;

        if (localPosition.x > 0)
        {
            return new Vector2(1, 0);
        }
        if (localPosition.x < 0)
        {
            return new Vector2(-1, 0);
        }
        if (localPosition.y > 0)
        {
            return new Vector2(0, 1);
        }
        if (localPosition.y < 0)
        {
            return new Vector2(0, -1);
        }

        return new Vector2(1, 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        enemyPlayer = collision.GetComponent<Player>();
        if (collision.tag.Equals("Player") && collision.GetComponent<Player>() != CurrentPlayer)
        {
            enemyPlayer.TakeDamage(BulletDamage, false);
            Destroy(this.gameObject);
            
        }
        else if (collision.tag.Equals("Wall"))
        {
            Destroy(this.gameObject);
        }
    }
}
