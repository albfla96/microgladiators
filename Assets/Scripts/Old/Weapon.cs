﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Weapon")]
public class Weapon : ScriptableObject
{
    public float bulletSpeed;
    public float damage;
    public float ammo;
    public Sprite xAxisPlayer1;
    public Sprite yAxisPlayer1;
    public Sprite xNegativeAxisPlayer1;
    public Sprite yNegativeAxisPlayer1;
    public Sprite xAxisPlayer2;
    public Sprite yAxisPlayer2;
    public Sprite xNegativeAxisPlayer2;
    public Sprite yNegativeAxisPlayer2;
}
