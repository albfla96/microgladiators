﻿using UnityEngine;

public class WeaponScript : MonoBehaviour
{
    private Player currentPlayer;
    private float damage;
    private float bulletSpeed;
    private float ammo;
    public Weapon weapon { get; set; }

    public KeyCode fire;
    private Transform firePoint;
    public GameObject bulletPrefab;

    private void Start()
    {
        currentPlayer = this.GetComponent<Player>();
        weapon = GameManager.INSTANCE.pistol;
        ResetAmmo();
    }

    private void Update()
    {
        if (Input.GetKeyDown(fire))
        {
            Fire();
        }
    }

    public void Fire()
    {
        ammo--;
        if (ammo < 0)
        {
            currentPlayer.SetDefaultWeapon();
            weapon = GetComponent<Player>().weapon;
            ResetAmmo();
        }

        firePoint = currentPlayer.FirePoint;
        var bulletGO = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Bullet bulletScript = bulletGO.GetComponent<Bullet>();

        bulletScript.CurrentPlayer = GetComponent<Player>();
        bulletScript.BulletDamage = weapon.damage;
        bulletScript.BulletSpeed = weapon.bulletSpeed;

        if (currentPlayer == GameManager.INSTANCE.Player2)
        {
            bulletScript.xAxis = weapon.xAxisPlayer1;
            bulletScript.yAxis = weapon.yAxisPlayer1;
            bulletScript.xNegativeAxis = weapon.xNegativeAxisPlayer1;
            bulletScript.yNegativeAxis = weapon.yNegativeAxisPlayer1;
            bulletGO.GetComponent<SpriteRenderer>().color = new Color(186, 0, 255, 255);
        } 
        else
        {
            bulletScript.xAxis = weapon.xAxisPlayer2;
            bulletScript.yAxis = weapon.yAxisPlayer2;
            bulletScript.xNegativeAxis = weapon.xNegativeAxisPlayer2;
            bulletScript.yNegativeAxis = weapon.yNegativeAxisPlayer2;
            bulletGO.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0, 255);
        }
    }

    public void ResetAmmo()
    {
        ammo = weapon.ammo;
    }
}
