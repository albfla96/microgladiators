using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class LaunchManager : MonoBehaviourPunCallbacks
{
    public static LaunchManager INSTANCE;

    public string username;
    public bool clearPrefs;
    public GameObject playerPrefab;

    #region UnityMethods
    private void Awake()
    {
        INSTANCE = this;
        if (clearPrefs)
        {
            DeletePlayerPrefs();
        }

        username = "username";
    }


    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        if (!PlayerPrefs.HasKey("PLAYERNAME"))
        {
            PhotonNetwork.NickName = SystemInfo.deviceName + "_" + SystemInfo.deviceModel;
            username = PhotonNetwork.NickName;
        }
        else
        {
            username = PlayerPrefs.GetString("PLAYERNAME");
        }

        LoadSettings();
    }

    public void DeletePlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion

    #region Public Methods

    public void LoadSettings()
    {
        if (PlayerPrefs.HasKey("PLAYERNAME"))
        {
            PhotonNetwork.NickName = PlayerPrefs.GetString("PLAYERNAME");
            ConnectToPhotonServers();
            PhotonNetwork.LoadLevel(1);
        }
    }

    public void ConnectToPhotonServers()
    {
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    public void InputName(string playerName)
    {
        if (string.IsNullOrEmpty(playerName))
        {
            return;
        }
        username = playerName;
    }

    public void SetPlayerName(string playerName)
    {
        PlayerPrefs.SetString("PLAYERNAME", username);
        PhotonNetwork.NickName = username;
        PlayerPrefs.SetInt("YourRank", 1);
        ConnectToPhotonServers();
        PhotonNetwork.LoadLevel(1);
    }

    #endregion

    #region PUN Callbacks

    public override void OnConnected()
    {
        Debug.Log(PhotonNetwork.NickName + " has connected to the Photon Servers");
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log(PhotonNetwork.NickName + " has connected to the Master Servers");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        Debug.Log(cause);
    }

    public override void OnJoinedLobby()
    {
        Debug.Log(PhotonNetwork.NickName + " has joined the lobby with " + PhotonNetwork.CountOfPlayers 
            + " players & " + PhotonNetwork.CountOfRooms + " rooms.");
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("We have joined " + PhotonNetwork.CurrentRoom.Name + " with " + PhotonNetwork.CurrentRoom.PlayerCount + " players.");
        PhotonNetwork.AutomaticallySyncScene = true;

        //Random spot to spawn
        int randSpawnPoint = Random.Range(-10, 10);

        //Spawn player into game scene
        PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(randSpawnPoint, randSpawnPoint, 0f), Quaternion.identity);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        //Matchmaking.INSTANCE.CreateRoomOnClick();
        Debug.Log(message);
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("New room was created.");
        Debug.Log("Room " + PhotonNetwork.CurrentRoom.Name + " was created.");

        //Matchmaking.INSTANCE.CreateRoomOnClick();
    }

    public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log("Player " + newPlayer.NickName + " has entered " + PhotonNetwork.CurrentRoom.Name);
    }

    public override void OnLeftRoom()
    {
        //Returning to loading screen after leaving the game
        PhotonNetwork.LoadLevel(1);
    }
    #endregion
}
