using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class LoadingScreenManager : MonoBehaviourPunCallbacks
{
    [Header("LScreenManager")]
    public static LoadingScreenManager INSTANCE;

    [Header("Spinning Circle")]
    public RectTransform imgRect;
    public float rotateSpeed = -200f;

    [Header("Loading Texts")]
    public string[] loadingTexts;
    public TextMeshProUGUI loadingTextDisplay;

    public Animator backgroundAnimator;
    public enum backgroundAnimType { BottomRight, BottomLeft, Center}
    public backgroundAnimType backgroundAnim;

    private void Awake()
    {
        INSTANCE = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ShowLoadingTexts());

        switch (backgroundAnim)
        {
            case backgroundAnimType.BottomRight:
                {
                    backgroundAnimator.SetInteger("BackgroundAnimation", 1);
                    break;
                }
            case backgroundAnimType.BottomLeft:
                {
                    backgroundAnimator.SetInteger("BackgroundAnimation", 2);
                    break;
                }
            case backgroundAnimType.Center:
                {
                    backgroundAnimator.SetInteger("BackgroundAnimation", 3);
                    break;
                }
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        imgRect.Rotate(0f, 0f, rotateSpeed * Time.deltaTime);

        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.LoadLevel(1);
        }
    }
    public IEnumerator ShowLoadingTexts()
    {
        for(int i = 0; i < loadingTexts.Length; i++)
        {
            loadingTextDisplay.text = loadingTexts[i];
            yield return new WaitForSeconds(3f);
        }

        Debug.Log("Loading the Main Menu");

        StartCoroutine(LoadScene("PlayerLobby"));
    }

    public IEnumerator LoadScene(string newScene)
    {
        yield return new WaitForSeconds(5f);
        PhotonNetwork.JoinLobby();
        PhotonNetwork.LoadLevel(newScene);
    }

    public override void OnConnected()
    {
        Debug.Log(PhotonNetwork.NickName + " has connected to the Photon Servers");
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log(PhotonNetwork.NickName + " has connected to the Master Servers");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        Debug.Log(cause);
    }

    public override void OnJoinedLobby()
    {
        Debug.Log(PhotonNetwork.NickName + " has joined the lobby with " + PhotonNetwork.CountOfPlayers
            + " players & " + PhotonNetwork.CountOfRooms + " rooms.");
    }
    public override void OnCreatedRoom()
    {
        Debug.Log("New room was created.");
        Debug.Log("Room " + PhotonNetwork.CurrentRoom.Name + " was created.");

        //Matchmaking.INSTANCE.CreateRoomOnClick();
    }

    public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log("Player " + newPlayer.NickName + " has entered " + PhotonNetwork.CurrentRoom.Name);
    }

    public override void OnLeftRoom()
    {
        //Returning to loading screen after leaving the game
        PhotonNetwork.LoadLevel(1);
    }
}
